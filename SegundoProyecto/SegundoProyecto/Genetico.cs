﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SegundoProyecto
{
    class Genetico
    {
        //Variable creada para almacenar la cantidad de comprobaciones que hará el algortimo.
        public static int comprobacionesGenetico = 0;

        //Variable creada para almacenar la cantidad de comprobaciones que hará el algortimo.
        public static int AsignacionesGenetico = 0;

        //Funcion de generacion de un random
        static Random randomGenerator = new Random();

        //Arreglo que almacena la cantidad de piezas que se colocaron en la matriz 1
        public static ArrayList listaPiezasEnMatriz1 = new ArrayList();

        //Arreglo que almacena la cantidad de piezas que se colocaron en la matriz 2
        public static ArrayList listaPiezasEnMatriz2 = new ArrayList();

        //Arreglo que almacena la cantidad de piezas que se colocaron en la matriz 2
        public static ArrayList listaCruces = new ArrayList();

        //Arreglo de piezas en el archivo
        public static ArrayList piezas = new ArrayList();

        //Variable de resultado del random
        static int pPieza = 0;

        //Variable que almacena la cantidad de lineas ejecutadas
        public static int lineasEjecutadasGenetico = 0;

        
        public static void imprimirCruces() {
            
            
            Console.WriteLine("\n\n lista de cruces \n\n ");
            for (int i = 0; i < listaCruces.Count; i++)
            {
              Console.WriteLine("Padre: " + listaPiezasEnMatriz1[i]+" "+" Madre: "+listaPiezasEnMatriz2[i]+" "+ " Hijo mejor " +listaCruces[i]);
            }
            
        }
      

        
        /*Funcion creada para seleccionar las mejores piezas de acuerdo a sus ponderaciones, estas son tomadas deacuedo a los espacios en blanco
         según como inicialmente se dibuja en matriz */
        public static int ponderacionesPiezas(int figura)
        {

            if (figura == 1 || figura == 2 || figura == 5 || figura == 8 || figura == 11 || figura == 12)
            {
                lineasEjecutadasGenetico += 2;
                return 4;
            }
            if (figura == 3 || figura == 4 || figura == 6 || figura == 7 || figura == 10 || figura == 13)
            {
                lineasEjecutadasGenetico += 2;
                return 3;
            }
            else
            {
                lineasEjecutadasGenetico += 1;
                return 2;
            }
        }

      
        /*Funcion encargada de elegir pieza, este metodo depende del anterior generarPiezaColocar() por motivo que se consulta la poderacion para saber
         * si la pieza es optima*/
        /*Funcion encargada de elegir pieza, este metodo depende del anterior por motivo que se consulta la poderacion para saber
         * si la pieza es optima*/
        public static int elegirPieza(int pieza1, int pieza2)
        {

            lineasEjecutadasGenetico += 2;
            int piezaElegida = 0;
            int ponderacionElegida = 0;


            int ponderacion = ponderacionesPiezas(pieza1);
            int ponderacion2 = ponderacionesPiezas(pieza2);
            lineasEjecutadasGenetico += 2;

            if (ponderacion >= ponderacion2)
            {
                ponderacionElegida = ponderacion;
                piezaElegida = pieza1;
                lineasEjecutadasGenetico += 3;
            }
            else
            {
                ponderacionElegida = ponderacion2;
                piezaElegida = pieza2;
                lineasEjecutadasGenetico += 3;
            }
           
            lineasEjecutadasGenetico++;
            return piezaElegida;
        }


        /*metodo encargado de ingresar las piezas al arraylist (Piezas), toma cada una de las piezas de la lista de piezas que se genero
         en el archivo*/

        public static void cargarArregloPiezas()
        {
            lineasEjecutadasGenetico += 1;
            piezas.Clear();
            for (int ingresarNumeros = 0; ingresarNumeros < Program.piezasArchivo.Count; ingresarNumeros++)
            {
                lineasEjecutadasGenetico += 2;
                piezas.Add(ingresarNumeros);
            }
        }

       
        /*Colocar Pieza solicita una pieza y verifica su disponibilidad el resultado de ese proceso lo aprovecha y coloca la pieza donde corresponda
         una vez evaluada su disponibilida de campo y estado de la pieza*/
        public static void crearMatriz3(int tamañoM)
        {
            Console.Write("\n\n         Matriz 3      \n\n");
            Program.espacios();
            Program.tamañoDeMatriz(tamañoM,tamañoM);
            Program.matrizInicial();
            escribirPiezaMatriz3();
            Program.imprimirMatriz(Program.matriz);
           
            Program.espacios();
        }

        /*Metodo encargado de escribir la pieza en la matriz 3 es decir, el resultado de los cruces de matriz 1 y matriz 2*/
        public static void escribirPiezaMatriz3()
        {
           
            int count = 0;
            int length = Program.matriz.GetLength(0);
            listaCruces.Clear();

            for (int i = length - 1; i >= 0; i--)
            {
                lineasEjecutadasGenetico++;
                for (int j = 0; j < length; j++)
                {
                    lineasEjecutadasGenetico += 2;

                    if (count >= listaPiezasEnMatriz1.Count && count >= listaPiezasEnMatriz2.Count)
                    {
                        break;
                    }

                    int figura = -1;
                    lineasEjecutadasGenetico += 3;
                    int figura1 = (int)listaPiezasEnMatriz1[count];//(int)listaElementosOptimos[count];//lista de mejores cruces
                    int figura2 = (int)listaPiezasEnMatriz2[count];

                    bool comprobacion1 = comprobarFiguraMatriz3(i, j, figura1);// comprueba si la figura puede guardarse en el espacio
                    bool comprobacion2 = comprobarFiguraMatriz3(i, j, figura2);// comprueba si la figura puede guardarse en el espacio

                    if (comprobacion1 == true && comprobacion2 == false)
                    {
                        figura = figura1;
                    }

                    else if(comprobacion1 == false && comprobacion2 == true){
                        figura = figura2;
                     
                    }

                    else if (comprobacion1 == true && comprobacion2 == true) {

                        figura = elegirPieza(figura1,figura2);
                    }

                   


                    if (figura != -1 )// selecciona la pieza a insertar y la dibuja en cada una de su estructuras
                    {
                        listaCruces.Add(figura);
                        count++;
                        if (figura == 1)
                        {
                            lineasEjecutadasGenetico += 4;
                            AsignacionesGenetico++;
                            Program.cantidadFigurasGeneradas++;
                            Program.cuadrado(i, j);
                        }
                        else if (figura == 2)
                        {

                            lineasEjecutadasGenetico += 4;
                            AsignacionesGenetico++;
                            Program.cantidadFigurasGeneradas++;
                            Program.rectanguloVertical(i, j);
                        }

                        else if (figura == 3)
                        {
                            AsignacionesGenetico++;
                            lineasEjecutadasGenetico += 4;
                            Program.cantidadFigurasGeneradas++;
                            Program.figuraVerdePosc1(i, j);


                        }
                        else if (figura == 4)
                        {
                            AsignacionesGenetico++;
                            lineasEjecutadasGenetico += 4;
                            Program.cantidadFigurasGeneradas++;
                            Program.rectanguloHorizontal(i, j);


                        }
                        else if (figura == 5)
                        {
                            AsignacionesGenetico++;
                            lineasEjecutadasGenetico += 3;
                            Program.cantidadFigurasGeneradas++;
                            Program.lDerecha(i, j);
                        }

                        else if (figura == 6)
                        {
                            AsignacionesGenetico++;
                            lineasEjecutadasGenetico += 4;
                            Program.cantidadFigurasGeneradas++;
                            Program.tAbajo(i, j);


                        }
                        else if (figura == 7)
                        {
                            AsignacionesGenetico++;
                            lineasEjecutadasGenetico += 4;
                            Program.cantidadFigurasGeneradas++;
                            Program.lAbajo(i, j);


                        }

                        else if (figura == 8)
                        {
                            AsignacionesGenetico++;
                            lineasEjecutadasGenetico += 4;
                            Program.cantidadFigurasGeneradas++;
                            Program.lizquierda(i, j);


                        }
                        else if (figura == 9)
                        {
                            AsignacionesGenetico++;
                            lineasEjecutadasGenetico += 4;
                            Program.cantidadFigurasGeneradas++;
                            Program.lArriba(i, j);


                        }

                        else if (figura == 10)
                        {
                            AsignacionesGenetico++;
                            lineasEjecutadasGenetico += 4;
                            Program.cantidadFigurasGeneradas++;
                            Program.tArriba(i, j);


                        }
                        else if (figura == 11)
                        {
                            AsignacionesGenetico++;
                            lineasEjecutadasGenetico += 4;
                            Program.cantidadFigurasGeneradas++;
                            Program.tDerecha(i, j);


                        }
                        else if (figura == 12)
                        {
                            AsignacionesGenetico++;
                            lineasEjecutadasGenetico += 4;
                            Program.cantidadFigurasGeneradas++;
                            Program.tIzquierda(i, j);


                        }

                        else if (figura == 13)
                        {
                            AsignacionesGenetico++;
                            lineasEjecutadasGenetico += 4;
                            Program.cantidadFigurasGeneradas++;
                            Program.figuraVerdeZ(i, j);

                        }
                    }
                }   
            }

        }

        /*Metodo encargado de verificar la disponibilidad de la pieza, en caso que sea disponible su campo retorna la pieza, y en caso de que
         * no lo sea intenta colocarla en otra posición en la matriz y que este disponible**/
        public static bool comprobarFiguraMatriz3(int i, int j,int figura)
        {
            lineasEjecutadasGenetico += 1;
            bool disponibilidadCampo = false;


            if (figura == 1)
                {
                    lineasEjecutadasGenetico += 3;
                    comprobacionesGenetico++;
                    disponibilidadCampo = ComprobacionFigura.comprobarCuadrado(i, j);
                }

            else if (figura == 2)
                {
                    lineasEjecutadasGenetico += 3;
                    comprobacionesGenetico++;
                    disponibilidadCampo = ComprobacionFigura.comprobarRectanguloV(i, j);

                }

            else if (figura == 3)
                {
                    lineasEjecutadasGenetico += 3;
                    comprobacionesGenetico++;
                    disponibilidadCampo = ComprobacionFigura.comprobarfiguraVerde(i, j);
                }

            else if (figura == 4)
                {
                    lineasEjecutadasGenetico += 3;
                    comprobacionesGenetico++;
                    disponibilidadCampo = ComprobacionFigura.comprobarRectanguloH(i, j);
                }

            else if (figura == 5)
                {
                    lineasEjecutadasGenetico += 3;
                    comprobacionesGenetico++;
                    disponibilidadCampo = ComprobacionFigura.comprobarLderecha(i, j);

                }
            else if (figura == 6)
                {
                    lineasEjecutadasGenetico += 3;
                    comprobacionesGenetico++;
                    disponibilidadCampo = ComprobacionFigura.comprobarTAbajo(i, j);
                }

            else if (figura == 7)
                {
                    lineasEjecutadasGenetico += 3;
                    comprobacionesGenetico++;
                    disponibilidadCampo = ComprobacionFigura.comprobarLAbajo(i, j);
                }

            else if (figura == 8)
                {
                    lineasEjecutadasGenetico += 3;
                    comprobacionesGenetico++;
                    disponibilidadCampo = ComprobacionFigura.comprobarLIzquierda(i, j);
                }

            else if (figura == 9)
                {
                    lineasEjecutadasGenetico += 3;
                    comprobacionesGenetico++;
                    disponibilidadCampo = ComprobacionFigura.comprobarLArriba(i, j);
                }
            else if (figura == 10)
                {
                    lineasEjecutadasGenetico += 3;
                    comprobacionesGenetico++;
                    disponibilidadCampo = ComprobacionFigura.comprobarTArriba(i, j);
                }
            else if (figura == 11)
                {
                    lineasEjecutadasGenetico += 3;
                    comprobacionesGenetico++;
                    disponibilidadCampo = ComprobacionFigura.comprobarTDerecha(i, j);
                }
            else if (figura == 12)
                {
                    lineasEjecutadasGenetico += 3;
                    comprobacionesGenetico++;
                    disponibilidadCampo = ComprobacionFigura.comprobarTIzquierda(i, j);
                }
            else if (figura == 13)
                {
                    lineasEjecutadasGenetico += 3;
                    comprobacionesGenetico++;
                    disponibilidadCampo = ComprobacionFigura.comprobarfiguraVerdeZ(i, j);
                }

         
            return disponibilidadCampo;
        }


       


        /*Metodo encargado de generar un random de piezas en el array de piezas (mismo del archivo)
         retorna la pieza en la posicion del random. */
        public static int generadorPieza()
        {
            lineasEjecutadasGenetico += 3;
            int p = randomGenerator.Next(0, piezas.Count);
            return (int)piezas[p];

        }



        /*Metodo encargado de verificar la disponibilidad de la pieza, en caso que sea disponible su campo retorna la pieza, y en caso de que
         * no lo sea intenta colocarla en otra posición en la matriz y que este disponible**/
        public static int solicitarPiezaMatriz1Y2(int i, int j)
        {
            lineasEjecutadasGenetico += 1;
            bool disponibilidadCampo;

            while (true)
            {
                if (piezas.Count == 0)
                {
                    lineasEjecutadasGenetico += 2;
                    break;
                }
                
                pPieza = generadorPieza();
                disponibilidadCampo = false;
                lineasEjecutadasGenetico += 2;


                if (pPieza == 1)
                {
                    lineasEjecutadasGenetico += 3;
                  
                    disponibilidadCampo = ComprobacionFigura.comprobarCuadrado(i, j);
                }

                else if (pPieza == 2)
                {
                    lineasEjecutadasGenetico += 3;
                   
                    disponibilidadCampo = ComprobacionFigura.comprobarRectanguloV(i, j);

                }

                else if (pPieza == 3)
                {
                    lineasEjecutadasGenetico += 3;
                    
                    disponibilidadCampo = ComprobacionFigura.comprobarfiguraVerde(i, j);
                }

                else if (pPieza == 4)
                {
                    lineasEjecutadasGenetico += 3;
                    
                    disponibilidadCampo = ComprobacionFigura.comprobarRectanguloH(i, j);
                }

                else if (pPieza == 5)
                {
                    lineasEjecutadasGenetico += 3;
                    
                    disponibilidadCampo = ComprobacionFigura.comprobarLderecha(i, j);

                }
                else if (pPieza == 6)
                {
                    lineasEjecutadasGenetico += 3;
                    
                    disponibilidadCampo = ComprobacionFigura.comprobarTAbajo(i, j);
                }

                else if (pPieza == 7)
                {
                    lineasEjecutadasGenetico += 3;
                   
                    disponibilidadCampo = ComprobacionFigura.comprobarLAbajo(i, j);
                }

                else if (pPieza == 8)
                {
                    lineasEjecutadasGenetico += 3;
                    
                    disponibilidadCampo = ComprobacionFigura.comprobarLIzquierda(i, j);
                }

                else if (pPieza == 9)
                {
                    lineasEjecutadasGenetico += 3;
                    
                    disponibilidadCampo = ComprobacionFigura.comprobarLArriba(i, j);
                }
                else if (pPieza == 10)
                {
                    lineasEjecutadasGenetico += 3;
                   
                    disponibilidadCampo = ComprobacionFigura.comprobarTArriba(i, j);
                }
                else if (pPieza == 11)
                {
                    lineasEjecutadasGenetico += 3;
                 
                    disponibilidadCampo = ComprobacionFigura.comprobarTDerecha(i, j);
                }
                else if (pPieza == 12)
                {
                    lineasEjecutadasGenetico += 3;
                   
                    disponibilidadCampo = ComprobacionFigura.comprobarTIzquierda(i, j);
                }
                else if (pPieza == 13)
                {
                    lineasEjecutadasGenetico += 3;
                    
                    disponibilidadCampo = ComprobacionFigura.comprobarfiguraVerdeZ(i, j);
                }

                if (disponibilidadCampo == false)
                {
                    
                    lineasEjecutadasGenetico += 3;
                   
                    piezas.Remove(pPieza);
                }
                else
                {
                    lineasEjecutadasGenetico++;
                    return pPieza;
                    
                }
            }
            lineasEjecutadasGenetico++;
            return -1;
        }


        /*Colocar Pieza solicita una pieza y verifica su disponibilidad el resultado de ese proceso lo aprovecha y coloca la pieza donde corresponda
         una vez evaluada su disponibilida de campo y estado de la pieza*/

        public static void colocarPieza(int identifMatriz)
        {
            lineasEjecutadasGenetico += 9;
            int length = Program.matriz.GetLength(0);
            for (int i = length - 1; i >= 0; i--)
            {
                lineasEjecutadasGenetico++; 
                for (int j = 0; j < length; j++)
                {
                    lineasEjecutadasGenetico += 2;
                    cargarArregloPiezas();
                    int figura = solicitarPiezaMatriz1Y2(i, j);

                    if (identifMatriz == 1 & figura != -1)
                    {
                        lineasEjecutadasGenetico++;
                        listaPiezasEnMatriz1.Add(figura);
                    }
                    else if (identifMatriz == 2 & figura != -1)
                    {
                        lineasEjecutadasGenetico++;
                        listaPiezasEnMatriz2.Add(figura);
                    }

                    if (figura == 1)
                    {
                        lineasEjecutadasGenetico += 3;
                    
                        Program.cantidadFigurasGeneradas++;
                        Program.cuadrado(i, j);
                    }
                    else if (figura == 2)
                    {

                        lineasEjecutadasGenetico += 3;
                        Program.cantidadFigurasGeneradas++;
                        Program.rectanguloVertical(i, j);
                    }

                    else if (figura == 3)
                    {

                        lineasEjecutadasGenetico += 3;
                        Program.cantidadFigurasGeneradas++;
                        Program.figuraVerdePosc1(i, j);


                    }
                    else if (figura == 4)
                    {

                        lineasEjecutadasGenetico += 3;
                        Program.cantidadFigurasGeneradas++;
                        Program.rectanguloHorizontal(i, j);


                    }
                    else if (figura == 5)
                    {

                        lineasEjecutadasGenetico += 3;
                        Program.cantidadFigurasGeneradas++;
                        Program.lDerecha(i, j);
                    }

                    else if (figura == 6)
                    {

                        lineasEjecutadasGenetico += 3;
                        Program.cantidadFigurasGeneradas++;
                        Program.tAbajo(i, j);


                    }
                    else if (figura == 7)
                    {

                        lineasEjecutadasGenetico += 3;
                        Program.cantidadFigurasGeneradas++;
                        Program.lAbajo(i, j);


                    }

                    else if (figura == 8)
                    {

                        lineasEjecutadasGenetico += 3;
                        Program.cantidadFigurasGeneradas++;
                        Program.lizquierda(i, j);


                    }
                    else if (figura == 9)
                    {

                        lineasEjecutadasGenetico += 3;
                        Program.cantidadFigurasGeneradas++;
                        Program.lArriba(i, j);


                    }

                    else if (figura == 10)
                    {

                        lineasEjecutadasGenetico += 3;
                        Program.cantidadFigurasGeneradas++;
                        Program.tArriba(i, j);


                    }
                    else if (figura == 11)
                    {

                        lineasEjecutadasGenetico += 3;
                        Program.cantidadFigurasGeneradas++;
                        Program.tDerecha(i, j);


                    }
                    else if (figura == 12)
                    {

                        lineasEjecutadasGenetico += 3;
                        Program.cantidadFigurasGeneradas++;
                        Program.tIzquierda(i, j);


                    }

                    else if (figura == 13)
                    {

                        lineasEjecutadasGenetico += 3;
                        Program.cantidadFigurasGeneradas++;
                        Program.figuraVerdeZ(i, j);

                    }
                }

            }

        }
    }
}
