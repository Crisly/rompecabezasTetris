﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SegundoProyecto
{
    class ComprobacionFigura
    {
        /*funcion encargada de verificar si el campo de la figura verde a insertar esta en 0 es decir desocupado, además hace comprobacion de las
 restricciones de la matriz*/

        public static Boolean comprobarfiguraVerde(int iBase, int jBase)
        {

            if ((iBase >= 2 && jBase < Program.matriz.GetLength(0) - 1))
            {
                return ((Program.matriz[iBase, jBase] != 0) && (Program.matriz[iBase, jBase + 1] == 0) && (Program.matriz[iBase - 1, jBase + 1] == 0) && (Program.matriz[iBase - 1, jBase] == 0) && (Program.matriz[iBase - 2, jBase] == 0));
            }
            else
            {
                return false;
            }

        }


        /*funcion encargada de verificar si el campo de la figura verde a insertar esta en 0 es decir desocupado, además hace comprobacion de las
    restricciones de la matriz*/

        public static Boolean comprobarfiguraVerdeZ(int iBase, int jBase)
        {

            if ((iBase > 0 && jBase < Program.matriz.GetLength(0) - 2))
            {
                return ((Program.matriz[iBase, jBase] != 0) && (Program.matriz[iBase, jBase + 1] == 0) && (Program.matriz[iBase, jBase + 2] == 0) && (Program.matriz[iBase - 1, jBase] == 0) && (Program.matriz[iBase - 1, jBase + 1] == 0));
            }
            else
            {
                return false;
            }

        }




        /*funcion encargada de verificar si el campo de la LArriba a insertar esta en 0 es decir desocupado, además hace comprobacion de las
      restricciones de la matriz*/
        public static Boolean comprobarLArriba(int iBase, int jBase)
        {

            if ((iBase >= 1 && jBase < Program.matriz.GetLength(0) - 2))
            {
                return ((Program.matriz[iBase, jBase] == 0) && (Program.matriz[iBase, jBase + 1] == 0) && (Program.matriz[iBase, jBase + 2] == 0) && (Program.matriz[iBase - 1, jBase + 2] == 0));

            }
            else
            {
                return false;
            }

        }

        /*funcion encargada de verificar si el campo de Lizquierda a insertar esta en 0 es decir desocupado, además hace comprobacion de las
      restricciones de la matriz*/
        public static Boolean comprobarLIzquierda(int iBase, int jBase)
        {

            if ((iBase >= 2 && jBase < Program.matriz.GetLength(0) - 1))
            {
                return ((Program.matriz[iBase, jBase] != 0) && (Program.matriz[iBase - 1, jBase] != 0) && (Program.matriz[iBase, jBase + 1] == 0) && (Program.matriz[iBase - 1, jBase + 1] == 0) && (Program.matriz[iBase - 2, jBase + 1] == 0) && (Program.matriz[iBase - 2, jBase] == 0));

            }
            else
            {
                return false;
            }

        }

        /*funcion encargada de verificar si el campo de la Lderecha a insertar esta en 0 es decir desocupado, además hace comprobacion de las
      restricciones de la matriz*/
        public static Boolean comprobarLderecha(int iBase, int jBase)
        {

            if ((iBase >= 2 && jBase < Program.matriz.GetLength(0) - 1))
            {
                return ((Program.matriz[iBase, jBase] == 0) && (Program.matriz[iBase, jBase + 1] == 0) && (Program.matriz[iBase - 1, jBase] == 0) && (Program.matriz[iBase - 2, jBase] == 0));

            }
            else
            {
                return false;
            }

        }

        /*funcion encargada de verificar si el campo de Labajo a insertar esta en 0 es decir desocupado, además hace comprobacion de las
      restricciones de la matriz*/

        public static Boolean comprobarLAbajo(int iBase, int jBase)
        {

            if ((iBase >= 1 && jBase < Program.matriz.GetLength(0) - 2))
            {
                return ((Program.matriz[iBase, jBase] == 0) && (Program.matriz[iBase - 1, jBase] == 0) && (Program.matriz[iBase - 1, jBase + 1] == 0) && (Program.matriz[iBase - 1, jBase + 2] == 0));
            }
            else
            {
                return false;
            }

        }


        public static Boolean comprobarRectanguloV(int iBase, int jBase)
        {
            if ((iBase >= 3))
            {
                return ((Program.matriz[iBase, jBase] == 0) && (Program.matriz[iBase - 1, jBase] == 0) && (Program.matriz[iBase - 2, jBase] == 0) && (Program.matriz[iBase - 3, jBase] == 0));
            }
            else
            {
                return false;
            }

        }

        /*funcion encargada de verificar si el campo del rectangulo horizontal a insertar esta en 0 es decir desocupado, además hace comprobacion de las
      restricciones de la matriz*/

        public static Boolean comprobarRectanguloH(int iBase, int jBase)
        {
            if ((jBase < Program.matriz.GetLength(0)-3))
            {
                return ((Program.matriz[iBase, jBase] == 0) && (Program.matriz[iBase, jBase + 1] == 0) && (Program.matriz[iBase, jBase + 2] == 0) && (Program.matriz[iBase, jBase + 3] == 0));
            }
            else
            {
                return false;
            }

        }

        /*funcion encargada de verificar si el campo de la TAbajo a insertar esta en 0 es decir desocupado, además hace comprobacion de las
      restricciones de la matriz*/
        public static Boolean comprobarTAbajo(int iBase, int jBase)
        {
            if ((iBase > 0 && jBase < Program.matriz.GetLength(0) - 2))
            {
                return ((Program.matriz[iBase, jBase] != 0) && (Program.matriz[iBase, jBase + 1] == 0) && (Program.matriz[iBase - 1, jBase] == 0) && (Program.matriz[iBase - 1, jBase + 1] == 0) && (Program.matriz[iBase - 1, jBase + 2] == 0));
            }
            else
            {
                return false;
            }

        }

        /*funcion encargada de verificar si el campo de la T arriba a insertar esta en 0 es decir desocupado, además hace comprobacion de las
      restricciones de la matriz*/
        public static Boolean comprobarTArriba(int iBase, int jBase)
        {
            if ((iBase >= 1 && jBase < Program.matriz.GetLength(0) - 2))
            {
                return ((Program.matriz[iBase, jBase] == 0) && (Program.matriz[iBase, jBase + 1] == 0) && (Program.matriz[iBase, jBase + 2] == 0) && (Program.matriz[iBase - 1, jBase + 1] == 0));
            }
            else
            {
                return false;
            }

        }

        /*funcion encargada de verificar si el campo de la Tizquierda a insertar esta en 0 es decir desocupado, además hace comprobacion de las
      restricciones de la matriz*/

        public static Boolean comprobarTIzquierda(int iBase, int jBase)
        {
            if ((iBase >= 2 && jBase < Program.matriz.GetLength(0) - 1))
            {
                return ((Program.matriz[iBase, jBase] != 0) && (Program.matriz[iBase, jBase + 1] == 0) && (Program.matriz[iBase - 1, jBase + 1] == 0) && (Program.matriz[iBase - 2, jBase + 1] == 0) && (Program.matriz[iBase - 1, jBase] == 0));
            }
            else
            {
                return false;
            }

        }

        /*funcion encargada de verificar si el campo de tDerecha a insertar esta en 0 es decir desocupado, además hace comprobacion de las
      restricciones de la matriz*/
        public static Boolean comprobarTDerecha(int iBase, int jBase)
        {
            if ((iBase >= 2 && jBase < Program.matriz.GetLength(0) - 1))
            {
                return ((Program.matriz[iBase, jBase] == 0) && (Program.matriz[iBase - 1, jBase] == 0) && (Program.matriz[iBase - 2, jBase] == 0) && (Program.matriz[iBase - 1, jBase + 1] == 0));

            }
            else
            {
                return false;
            }

        }

        /*funcion encargada de verificar si el campo del cuadrado a insertar esta en 0 es decir desocupado, además hace comprobacion de las
      restricciones de la matriz*/
        public static Boolean comprobarCuadrado(int iBase, int jBase)
        {
            if ((iBase > 0 && jBase < Program.matriz.GetLength(0) - 1))
            {

                return ((Program.matriz[iBase, jBase] == 0) && (Program.matriz[iBase, jBase + 1] == 0) && (Program.matriz[iBase - 1, jBase] == 0) && (Program.matriz[iBase - 1, jBase + 1] == 0));

            }
            else
            {
                return false;
            }

        }
        
    }
}
